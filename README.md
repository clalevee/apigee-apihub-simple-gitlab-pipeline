# Google Apigee & Api Hub Gitlab Pipeline

![Generic badge](https://img.shields.io/badge/status-beta-yellow.svg)


**This is not an official Google product.**<BR>This implementation is not an official Google product, nor is it part of an official Google product. Support is available on a best-effort basis via GitHub.

***

## Goal

Implementation of a CI/CD pipeline for Google Apigee and API Hub using
[GitLab-CI](https://docs.gitlab.com/ee/ci/introduction/) and Apigee Maven Plugins.


### The CICD pipeline includes:

- An Apigee proxy to be deployed in Apigee X/hybrid (Edge/OPSK not supported)
    - Folder **apiProxy**
    - Static Apigee Proxy code analysis using [apigeelint](https://github.com/apigee/apigeelint)
    - Static JS code analysis using [eslint](https://eslint.org/)
    - Unit JS testing using [mocha](https://mochajs.org/)
    - Integration testing of the deployed proxy using [apickli](https://github.com/apickli/apickli)
    - Packaging and deployment of the API proxy bundle using
    [Apigee Deploy Maven Plugin](https://github.com/apigee/apigee-deploy-maven-plugin)

- A corresponding API specification file (OAS, Open Api Specification) and its configuration file to be published on Apigee Api Hub (several versions)
    - Folders **apiHub** and **demo_files**
    - Publication of the API into Apigee Api Hub using
    [Apigee Registry Maven Plugin](https://github.com/apigee/apigee-registry-maven-plugin)


### Pipeline Principle

![CICD Pipeline overview](./images/project.jpg)

3 pipelines ([GitLab Parent-child pipelines](https://docs.gitlab.com/ee/ci/pipelines/pipeline_architectures.html#parent-child-pipelines): 1 parent, 2 children):
   - /.gitlab-ci.yml: parent pipeline, triggering apiProxy pipeline and apiHub pipeline 
   - /apiProxy/.gitlab-ci.yml: Deploy proxy to Apigee if it has changed
   - /apiHub/.gitlab-ci.yml: Publish the specification on API Hub if it has changed
  

## Limitations & Requirements

- The authentication to the Apigee X / Apigee hybrid API is done using a GCP Service Account. See CI/CD Configuration.
- Apigee Hub must be provisioned. See [Api Hub Documentation](https://cloud.google.com/apigee/docs/api-hub/get-started-api-hub)

***

<BR>

## CI/CD Configuration Instructions

### Initialize a GitLab Repository

Create a GitLab repository to hold your API Proxy. 

To use the `Apigee-ApiHub-Simple-GitLab-Pipeline`
in your GitLab repository like `gitlab.com/my-user/my-apihub-proxy-repo`, follow these
steps:

```bash
git clone git@gitlab.com:clalevee/apigee-apihub-simple-gitlab-pipeline.git
cd apigee-apihub-simple-gitlab-pipeline
git init
git remote add origin git@github.com:my-user/my-apihub-proxy-repo.git
git checkout -b feature/cicd-pipeline
git add .
git commit -m "initial commit"
git push -u origin feature/cicd-pipeline
```

### Google Cloud: Create Service Account

Apigee X deployement requires a GCP Service Account with the following roles (or a custom role with all required permissions):

- Apigee Environment Admin
- Cloud Apigee Registry Editor

To create it in your Apigee organization's GCP project, use following gcloud commands (or GCP Web UI):

```sh
SA_NAME=<your-new-service-account-name>

gcloud iam service-accounts create $SA_NAME --display-name="GitLab Service Account"

PROJECT_ID=$(gcloud config get-value project)
GitLab_SA=$SA_NAME@$PROJECT_ID.iam.gserviceaccount.com

gcloud projects add-iam-policy-binding "$PROJECT_ID" \
  --member="serviceAccount:$GitLab_SA" \
  --role="roles/apigee.environmentAdmin"

gcloud projects add-iam-policy-binding "$PROJECT_ID" \
  --member="serviceAccount:$GitLab_SA" \
  --role="roles/apigee.apiAdmin"

gcloud projects add-iam-policy-binding "$PROJECT_ID" \
  --member="serviceAccount:$GitLab_SA" \
  --role="roles/apigeeregistry.editor"

gcloud iam service-accounts keys create $SA_NAME-key.json --iam-account=$GitLab_SA --key-file-type=json 

```

Copy `<your-new-service-account-name>-key.json` file content to clipboard. 

### GitLab Repository Configuration 

Add custom environment variables, `GCP_SERVICE_ACCCOUNT`, to store your GCP Service Account json key:
- Go to your project’s Settings > CI/CD and expand the Variables section.
- Click the **Add Variable** button.<BR>In the Add variable modal, fill in the details:
  - Key: GCP_SERVICE_ACCCOUNT
  - Value: paste clipboard
  - Type: File
  - Environment scope: All
  - Protect variable (Optional): If selected, the variable is only available in pipelines that run on protected branches or tags.
  - Mask variable (Optional): If selected, the variable’s Value is masked in job logs. The variable fails to save if the value does not meet the masking requirements.
  - Click the **Add Variable** button

***

<BR>

## Run the demo

### Demo scenario

The pipeline illustrates SDLC for both OAS file and corresponding Apigee proxy (airport-cicd). 

4 steps: 

  - Stage 1: New specification version 1.0.0, API is in **design** stage
  - Stage 2: Specification version 1.0.0, API is in **develop** stage and deployed (Apigee proxy is deployed)
  - Stage 3: New specification version 1.0.1, API is in **develop** stage, but updated (Apigee proxy is updated). Version 1.0.0 is **retired**
  - Stage 4: New specification version 2.0.0 in **design** stage

![GitHub CICD Pipeline overview](./images/api-sdlc.jpg)

<BR>

> Note: 
> 
>  - the **demo_files** folder contains specification files and Api Hub configuration files needed for each stages.
>  - Update **demo_files/apiHub/_api_config * .yaml** files to add your own references to your environment components (endpoint hostname, developer portal URL, Apigee dashboard URL, ...)

<BR>

### Set pipeline variables

Using your favorite IDE...

- Update the **/apiProxy/.gitlab-ci.yml** file.<BR>
In **Variables** section, change `APIGEE_ORG`, `APIGEE_ENV` and `TEST_HOST` values by your target Apigee organization values. Save it.
- Update the **/apiHub/.gitlab.ylm** file.<BR>
In **Variables** section, change `GCP_PROJECT` by your GCP Project name. Save it.


***

### Stage 1: API version 1.0.0 (Design Status)

1.  Update the **apiHub / specs / airport-1.0.0.yaml** file: set **host** with your Apigee environment hostname.
2. Save
3. Commit, Push

Use the GitLab UI to monitor your pipeline execution and read test reports (in pipeline artifact):

- Go to your GitLab project > **CI/CD** > **Pipeline**... <BR> 
- Click on the running pipeline
- You should see only one trigger child pipeline running (**Apigee-apiHub**), as:
    - Apigee proxy was not updated/commited
    - API is updated: API version 1.0.0 in **design** status


![GitLab-CI Pipeline overview](./images/step1.1.jpg)

<BR>


- Expand pipeline jobs, then click on **Job** to see execution detail. In list of jobs, click on the job you want to see logs.

![GitLab-CI Pipeline overview](./images/step1.2.jpg)

<BR>

Open API Hub service console and verify that the API 1.0.0 was published in **design** status, with no Deployment.

  ![GitLab-CI Pipeline overview](./images/step1.3.jpg)

<BR>

***

### Stage 2: : version 1.0.0 (Develop Status, Deployed)

1.  Replace content of file **apiHub / api-config.yaml** file with content of file **demo_file / apiHub / 2_api-config-dev-1.0.yaml**.
2.  Copy folders  **demo_file / apiProxy / apiproxy** and **demo_file / apiProxy / test** to **apiProxy** folder
3. Commit, Push

<BR>

Use the GitLab UI to monitor your pipeline execution and read test reports (in pipeline artifact):

- Go to your GitLab project > **CI/CD** > **Pipeline**... <BR> 
- Click on the running pipeline <BR> 
You should see both **apiProxy** pipeline and **apiHub** pipelines running, one then the other, as:
  - Apigee proxy was updated 
  - API is updated: API version 1.0.0 in **develop** status and deployed

Click on the pipelines to monitor executions.

  ![GitLab-CI Pipeline overview](./images/step2.1.jpg)

Open Apigee console and verify that the **airports-cicd** was deployed in the right environment.

  ![GitLab-CI Pipeline overview](./images/step2.2.jpg)


<BR>

***

### Stage 3: version 1.0.1 (Develop Status, Deployed), version 1.0.0 (Retired)

1.  Copy the **demo_file / apiHub / specs / airport-1.0.1.yaml** to **apiHub / specs** folder
2.  Update the **apiHub / specs / airport-1.0.1.yaml** file: set **host** with your Apigee environment hostname.
3. Save
4.  Replace content of file **apiHub / api-config.yaml** file with content of file **demo_file / apiHub / 3_api-config-dev-1.1.yaml**.
5. Commit, Push

<BR>

Use the GitLab UI to monitor your pipeline execution and read test reports (in pipeline artifact):

- Go to your GitLab project > **CI/CD** > **Pipeline**... <BR> 
- Click on the running pipeline <BR> 
You should see only **apiHub** pipeline running, as: 
  - Apigee proxy was not updated (from Apigee proxy point of view, no difference between API v1.0.0 and API v1.0.1)
  - API is updated: API version 1.0.1 in **Develop** status, and move API 1.0.0 in **Retired** status

Click on the pipeline to monitor executions.


Open API Hub service console and verify that the API 1.0.1 was published in **Develop** status, with one Deployment, and API 1.0.0 was **retired**.

 ![GitLab-CI Pipeline overview](./images/step3.1.jpg)

<BR>

***

### Stage 4: version 1.0.1 (Develop Status, Deployed), version 1.0.0 (Retired), version 2.0.0 (Design Status)

1.  Copy the **demo_file / apiHub / specs / airport-2.0.0.yaml** to **apiHub / specs** folder
2.  Update the **apiHub / specs / airport-2.0.0.yaml** file: set **host** with your Apigee environment hostname.
3. Save
4. Replace content of file **apiHub / api-config.yaml** file with content of file **demo_file / apiHub / 4_api-config-design-2.0.yaml**
5. Commit, Push

<BR>

- Go to your GitLab project > **CI/CD** > **Pipeline**... <BR> 
- Click on the running pipeline <BR> 
You should see only **apiHub** pipeline running, as: 
  - Apigee proxy was not updated (from Apigee proxy point of view, no difference between API v1.0.0 and API v1.0.1)
  - API is updated: Api version 2.0.0 in **design** status

Click on the pipeline to monitor executions.

 Open API Hub service console and verify that the API 1.0.1 was published in **Develop** status, with one Deployment, and API 1.0.0 was **retired**, and API 2.0.0 was in **Design**.

  ![GitLab-CI Pipeline overview](./images/step4.1.jpg)

<BR>